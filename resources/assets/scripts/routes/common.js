export default {
  init() {
    // JavaScript to be fired on all pages
    $('.fixed-top').addClass('navbar-hide');
    

    // for initial state
    if ( window.matchMedia('(min-width: 767px)').matches) {
      $(window).scroll(function() {
        // add/remove class to navbar when scrolling to hide/show
        var scroll = $(window).scrollTop();
        if (scroll > 150 && window.matchMedia('(min-width: 767px)').matches) {
          $('.fixed-top').removeClass('navbar-hide');
        } else {
          $('.fixed-top').addClass('navbar-hide');
        }
      })
    } else {
      $('.fixed-top').addClass('navbar-hide');
    }
    
    // for browser resizing
    $(window).resize(function() {
      if ( window.matchMedia('(min-width: 767px)').matches) {
        $(window).scroll(function() {
          // add/remove class to navbar when scrolling to hide/show
          var scroll = $(window).scrollTop();
          if (scroll > 150 && window.matchMedia('(min-width: 767px)').matches) {
            $('.fixed-top').removeClass('navbar-hide');
          } else {
            $('.fixed-top').addClass('navbar-hide');
          }
        })
      } else {
        $('.fixed-top').addClass('navbar-hide');
      }
    })
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired

    // $('body').scrollspy({target: '#side-nav', offset: 500});
    // $('body').scrollspy({target: '.toTop', offset: 0});

    // Add smooth scrolling on all links inside the navbar
    $('.nav-link, #toTop').on('click', function(event) {

      // Make sure this.hash has a value before overriding default behavior
      if (this.hash !== '') {

        // Prevent default anchor click behavior
        event.preventDefault();

        // Store hash
        var hash = this.hash;

        // Using jQuery's animate() method to add smooth page scroll
        $('html, body').animate({
          scrollTop: $(hash).offset().top,
        }, 800, function(){

        // Add hash (#) to URL when done scrolling (default click behavior)
          window.location.hash = hash;
        });
      } // End if
    }); 

      // toTop function
    $(window).scroll(function(){
      if ($(window).scrollTop() > 150) {
        $('.mobile-brand').addClass('fixed-nav');
      } else {
        $('.mobile-brand').removeClass('fixed-nav');
      }
      if ($(window).scrollTop() > 250) {
        $('.toTop-container').fadeIn();
      } else {
        $('.toTop-container').fadeOut();
      } 
    });

    $('#toTop').click(function() {
      $('html, body').animate({scrollTop : 0},500);
    });
  },
};