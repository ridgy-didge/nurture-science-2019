@extends('layouts.app')

@section('content')
  <section class="wrap welcome-wrap">
    <article class="content container">
      <h1 class="archive-title"> @include('partials.page-header')</h1>
      <img class="welcome-bg-image" src="@asset('images/nurture-science-white.svg')" />
    </article>
  </section>
  @include('partials.categories-list')
  @include('partials.search')
  @include('partials.donate-container')
  @include('partials.contact-container')
  @include('partials.posttypes')
  @include('partials.participate-container')
  @include('partials.subscribe-container')
  {!! get_the_posts_navigation() !!}
@endsection
