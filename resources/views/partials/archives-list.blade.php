<div id="archives" class="wrap archive-wrap" role="document">
  @php
      $selected_val = 'sort'; // set inital state
      $selected_val_order = 'ASC'; // set inital state
      if(isset($_POST['submit'])){
        $selected_explode = explode(" ", $_POST['filter']); // split the seleced value
        $selected_val =  $selected_explode[0];  // update filter option
        $selected_val_order = $selected_explode[1];  // update filter option
      }
    @endphp
    @php 
      $postID = get_post_type();
      $posttypes_query = new WP_Query( 
        array(
          'post_type' => $postID,
          'post_status' => 'publish',
          'post__not_in' => array( 1118, 1120, 1122, 1124, 1126, 1127, 1128, 1129, 1130 ),
          'orderby' => $selected_val,
          'order'   => $selected_val_order,
        ) 
      ); 
      $obj = get_post_type_object( get_post_type( get_the_ID() ) );
      $posttype = $obj->labels->singular_name; 
  @endphp
  <div class="container archive-container" role="document">
    <div class="archive-container-header">
      <div class="archive-container-title">
        <h3>Latest Articles For @php echo $posttype @endphp</h3>
      </div>
      @include('partials.sort-form')
    </div>
    @php if ( $posttypes_query->have_posts() ) : while ( $posttypes_query->have_posts() ) : $posttypes_query->the_post(); @endphp
      <article class="archive-article">
        <h2 class="type-title">
        @php
          echo $posttype
        @endphp
        </h2>
        <h3>@php echo the_title() @endphp</h3>
        <div class="archive-article-excerpt">
          @php the_excerpt(); @endphp
        </div>
        <div class="archive-article-efooter">
          @include('partials.authors')
        </div>
      </article>
      @endwhile
    @else
      <p>No articles found.</p>
    @php endif; wp_reset_query(); @endphp
  </div>
</div>
