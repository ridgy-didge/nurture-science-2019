
@if ( function_exists( 'get_coauthors' ) )
  <div class="authors-container">
    <div class="authors-list">
      <div class="authors-title-container">
        @php 
          $coauthors = get_coauthors();
          $coauthorsCount = count((array)$coauthors); 
        @endphp
        @if ( $coauthorsCount > 1 )
          <p class="authors-title">Authors: </p>
        @else
          <p class="authors-title">Author: </p>
        @endif
      </div>
      @foreach ( $coauthors as $coauthor )
        <div id="" class="author">
          @php 
            $archive_link = get_author_posts_url( $coauthor->ID, $coauthor->user_nicename );
            $link_title = 'Posts by ' . $coauthor->display_name;
          @endphp
          <a class="author-link" href="<?php echo esc_url( $archive_link ); ?>" title="<?php echo esc_attr( $link_title ); ?>">
            @php echo $coauthor->display_name @endphp
          </a>
        </div>
      @endforeach
    </div> 
  </div>
@else  
  @php 
    $archive_link = get_author_posts_url( get_the_author_meta( 'ID' ) );
    $link_title = 'Posts by ' . the_author();
  @endphp
  <a class="author-link" href="<?php echo esc_url( $archive_link ); ?>" title="<?php echo esc_attr( $link_title ); ?>">
@endif