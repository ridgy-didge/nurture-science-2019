@php  
  $category = get_queried_object();
@endphp

<div class="wrap categories-wrap" role="document">

  <div class="container categories-container" role="document">
    <div class="categories-container-header">
      <div class="categories-container-title">
        <h3>Articles for Category @php echo $category->name @endphp</h3>
      </div>
      @include('partials.sort-form' )
    </div>
    @php
      $selected_val = 'date'; // set inital state
      if(isset($_POST['submit'])){
      $selected_val = $_POST['filter'];  // update filter option
      }
    @endphp

    @php 
      $posttypes_query = new WP_Query( 
        array(
          'post_type' => array( 'pregnancy', 'newborns', 'baby', 'toddlers','yngadults', 'adults', 'elderly'),
          'post_status' => 'publish',
          'orderby' => $selected_val,
          'order'   => 'ASC',
          'category_name' => $category->slug,
        ) 
      ); @endphp
      @php if ( $posttypes_query->have_posts() ) : while ( $posttypes_query->have_posts() ) : $posttypes_query->the_post(); @endphp
        <article class="categories-article">
          <h2 class="type-title">
          @php
            $obj = get_post_type_object( get_post_type( get_the_ID() ) );
            $posttype = $obj->labels->singular_name; 
            echo $posttype
          @endphp
          </h2>
          <h3>@php echo the_title() @endphp</h3>
          <div class="categories-article-excerpt">
            @php the_excerpt(); @endphp
          </div>
          <div class="categories-article-footer">
            @php /* echo get_avatar( get_the_author_email(), '128', '/images/no_images.jpg', get_the_author() ); */ @endphp
            @include('partials.authors')
          </div>
        </article>
        @php endwhile; endif; wp_reset_query(); 
      @endphp
  </div>
</div>
