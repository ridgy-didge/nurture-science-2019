<section id="contact" class="wrap contact-container">
  <div class="content container contact-title-holder">
    <h3 class="area-title">ASK NURTURE SCIENCE</h3>
  </div>
  <div class="wrap contact-body-holder">
    <div class="content container">
    @php $participate_query = new WP_Query( 
        array(
          'p' => 602,  // ID for "Question to Nurture Science"
          'post_type' => 'any', // from any post type
          'orderby' => 'date',
          'order'   => 'DESC',
        ) 
        ); @endphp
        @php if ( $participate_query->have_posts() ) : while ( $participate_query->have_posts() ) : $participate_query->the_post(); @endphp
          <article class="contact-article">
            <?php the_excerpt(); ?>
            <a class="contact-button" href="<?php echo get_permalink(); ?>" >Ask a question</a>
          </article>
        @php endwhile; endif; wp_reset_query(); @endphp
    <div>
  </div>
</section>
