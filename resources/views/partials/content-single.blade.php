<article @php post_class() @endphp>
  <header>
    <h1 class="entry-title">{!! get_the_title() !!}</h1>
    <p>Created on: <time class="updated" datetime="{{ get_post_time('c', true) }}">{{ get_the_date() }}</time></p>
  </header>
  <div class="entry-content">
    @php the_content() @endphp
  </div>
  <footer>
    {!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']) !!}
  </footer>
  @php
    $pt = get_post_type_object( get_post_type() ); 
    $labelTitle = $pt->labels->singular_name;
  @endphp
  @if ( $labelTitle != 'Site Partner' )
  <div class="entry-footer">
      <div class="tags-wrapper">
        <h3 class="area-title">TAGS: </h3>
        <div class="tags-container">
        @php echo get_the_tag_list(' '); @endphp
        </div>
      </div>
      <div class="categories-wrapper">
        <h3 class="area-title">CATEGORIES: </h3>
        <div class="categories-container">
        @php the_category( ' ' ); @endphp
        </div>
      </div>
    </div>
  @endif
 
  @if ( $labelTitle != 'Site Partner' )
    @if ( have_rows('article_references') )
      <div class="reference-container">
        <div class="reference-header">
          <h4 class="area-title references-title">Article References</h4>
          <div class="reference-toggle-holder">
            <a id="arrow" class="btn-toTop" data-toggle="collapse" href="#collapseReference" aria-expanded="false" aria-controls="collapseReference">
              @include('svg.to-top')
            </a>
          </div>
        </div>
        <div class="collapse" id="collapseReference">
          <div class="reference-description">
            <?php // loop through the rows of data
            while ( have_rows('article_references') ) : the_row(); ?>
              <h3 class="entry-title">
                <?php
                the_sub_field('article_reference_title');
                ?>
              </h3>
              <?php
              the_sub_field('article_reference_description');
              ?>
              <?php // display a sub field value
            endwhile; ?>
          </div> 
        </div> 
      </div>
      @else
        <p>no rows found</p>
      @endif
    @php comments_template('/partials/comments.blade.php') @endphp
  @endif
</article>

