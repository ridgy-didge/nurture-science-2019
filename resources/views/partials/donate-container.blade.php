<section id="donate" class="wrap donate-container">
  <div class="content container donate-title-holder">
    <h3 class="area-title">DONATE</h3>
  </div>
  <div class="wrap donate-body-holder">
    <div class="content container">
      @php $participate_query = new WP_Query( 
        array(
          'p' => 693,  // participate ID
          'post_type' => 'any', // from any post type
        ) 
        ); 
      @endphp
      @php if ( $participate_query->have_posts() ) : while ( $participate_query->have_posts() ) : $participate_query->the_post(); @endphp
        <article class="donate-article">
          <?php the_excerpt(); ?>
          <a class="donate-button" href="<?php echo get_permalink(); ?>" >Donate Here</a>
        </article>
      @php endwhile; endif; wp_reset_query(); @endphp
    <div>
  </div>
</section>
