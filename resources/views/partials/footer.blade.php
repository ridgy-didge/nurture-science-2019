<section id="footerLinks" class="wrap footer-links-container">
  <div class="container">
    <section class="footer-links-useful">
      <h3>Useful Links</h3>
      <ul>
        <li><a class="footer-useful-link" href="<?php echo site_url() ?>/about-us">About Nurture Science</a></li>
        <li><a class="footer-useful-link" href="<?php echo site_url() ?>/contact-us">Contact Us</a></li>
      </ul>
    </section>
    <section class="footer-links-social">
      @php dynamic_sidebar('sidebar-footer') @endphp
    </section>
    <section class="footer-links-foot">
      <div>
        <a href="<?php echo site_url() ?>/donate-to-this-site" class="btn-grn btn-donate">Donate</a>
      </div>
    </section>

  </div>
</section>

<footer class="copyright-info">
  <section class="container">
    <p>&copy; Copyright <?php echo get_bloginfo( 'name' ) ." " . date("Y"); ?>
  </section>
</footer>

