<header class="banner">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navContainerMain">
    <svg id="toggle-button" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 200 100">
      <title>nav-toggle</title>
      <path class="cls-3" d="M150,6a44,44,0,0,1,0,88H50A44,44,0,0,1,50,6H150m0-6H50A50.15,50.15,0,0,0,0,50H0a50.15,50.15,0,0,0,50,50H150a50.15,50.15,0,0,0,50-50h0A50.15,50.15,0,0,0,150,0Z"/>
      <g class="nav-toggle-btn" >
        <circle class="cls-1" cx="50" cy="50" r="39.5"/>
        <path class="cls-2" d="M50,13.5A36.5,36.5,0,1,1,13.5,50,36.54,36.54,0,0,1,50,13.5m0-6A42.5,42.5,0,1,0,92.5,50,42.5,42.5,0,0,0,50,7.5Z"/>
      </g>
    </svg>
  </button>

  <div class="modal fade" id="seachModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <p>Search Nurture Science</p>
        </div>
        <div class="modal-body">  
          @include('partials.search')
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <div class="mobile-brand">
    <div class="brand-container">
      <a class="brand" href="{{ home_url('/') }}"><img alt="magnifying glass icon" src="@asset('images/nurture-science-blue.svg')" /></a>
      <div class="mobile-search">
        <button type="button" class="mobile-modal-toggle" data-toggle="modal" data-target="#seachModal">
          <img alt="search icon" src="@asset('images/icons/search-icon-01.svg')" />
          <p>Search</p>
        </button>
      </div>
      <div class="brand-description"><p><?php bloginfo('description'); ?></p> </div>
    </div>
    <div id="navContainerMain" class="collapse">
      <div id="navPrimaryMain" >
        @include('partials.megamenu')
      </div>
    </div>
  </div>

  <div class="container desktop-brand">
    <div class="brand-container">
      <a class="brand" href="{{ home_url('/') }}"><img alt="magnifying glass icon" class="brand-image" src="@asset('images/nurture-science-blue.svg')" /></a>
      <div class="brand-description"><p><?php bloginfo('description'); ?></p> </div>
    </div>
    <div class="desktop-search">
      <div class="search-container">
        @include('partials.search')
      </div>
    </div>
    <div id="navContainer" class="collapse navContainer">
      <div id="navPrimary" >
        @include('partials.megamenu')
      </div>
    </div>
  </div>
</header>

@include('partials.to-top')

<div class="nav-holder fixed-top">
  <div class="container">
    <a aria-label="mobile view logo button" class="brand" href="{{ home_url('/') }}"><img alt="nurture science logo" src="@asset('images/nurture-science-white.svg')" /></a>
    @if (has_nav_menu('post_types_menu'))
      {!! wp_nav_menu(['theme_location' => 'post_types_menu', 'menu_class' => 'nav']) !!}
    @endif
  </div>     
</div>
