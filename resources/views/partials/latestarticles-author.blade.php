<section id="latestauthorearticles" class="wrap latest-author-articles-container">
  <div class="content container latest-author-articles-holder">
    <div class="archive-container-title">
        <h3 class="archive-title"> Articles from @php the_author_meta( 'display_name' ) @endphp</h3>
    </div>
    @php 
      $authorID = get_the_author_meta( 'user_nicename' );
      $postID = get_post_type();
      $posttypes_query = new WP_Query( 
        array(
          'post_type' => array( 'pregnancy', 'newborns', 'baby', 'toddlers', 'children', 'yngadults', 'teens', 'adults', 'elderly'),
          'post_status' => 'publish',
          'post__not_in' => array( 1118, 1120, 1122, 1124, 1126, 1127, 1128, 1129, 1130 ),
          'order'   => 'ASC',
          'author_name' => $authorID,
          'posts_per_page' => 8
        )
      ); 
    @endphp
    @php if ( $posttypes_query->have_posts() ) : while ( $posttypes_query->have_posts() ) : $posttypes_query->the_post(); @endphp
      <article class="archive-article">
        <h2 class="type-title">
        @php
          $obj = get_post_type_object( get_post_type( get_the_ID() ) );
          $posttype = $obj->labels->singular_name; 
          echo $posttype
        @endphp
        </h2>
        <h3>@php echo the_title() @endphp</h3>
        <div class="archive-article-excerpt">
          @php the_excerpt(); @endphp
        </div>
        <div class="archive-article-efooter">
          @include('partials.authors')
        </div>
      </article>
      @php endwhile; endif; wp_reset_query();  @endphp
  </div>
</div>
