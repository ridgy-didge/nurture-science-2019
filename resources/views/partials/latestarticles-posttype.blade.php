
<section id="latestpostypearticles" class="wrap latest-articles-container latest-posttype-articles-posttype-container">
  @php 
  $postID = get_post_type();      
  $posttypes_query = new WP_Query( 
    array(
      'post_type' => $postID,
      'post_status' => 'publish',
      'posts_per_page' => 1,
      'post__not_in' => array( 1118, 1120, 1122, 1124, 1126, 1127, 1128, 1129, 1130 ),
      'orderby' => 'date',
      'order'   => 'ASC',
    ) 
  );
  $obj = get_post_type_object( get_post_type( get_the_ID() ) );
  $posttype = $obj->labels->singular_name; 
  @endphp

  <div class="content container latest-posttype-articles-holder">
    <div class="area-title">
      <p>LATEST ARTICLE FOR @php echo strtoupper($posttype) @endphp</p>
    </div>
   
        @php if ( $posttypes_query->have_posts() ) : while ( $posttypes_query->have_posts() ) : $posttypes_query->the_post(); @endphp
          <article class="latest-article">
            <h2 class="type-title">
            @php echo $posttype @endphp
            </h2>
            <h3>@php the_title(); @endphp</h3>
            @php the_excerpt(); @endphp
            <div class="latest-article-footer">
              @include('partials.authors')
            </div>
          </article>
          @endwhile
        @else
          <p>No articles found.</p>
        @php endif; wp_reset_query(); @endphp
  </div>
</section>
