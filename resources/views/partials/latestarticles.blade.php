
 <section id="latestarticles" class="wrap latest-articles-container">
    <div class="content container latest-articles-holder">
      <p class="area-title">LATEST ARTICLES</p>
      @php $posttypes_query = new WP_Query( 
        array(
          'post_type' => array( 'pregnancy', 'newborns', 'baby', 'toddlers', 'children', 'yngadults', 'teens', 'adults', 'elderly'),
          'post_status' => 'publish',
          'posts_per_page' => 4,
          'post__not_in' => array( 1118, 1120, 1122, 1124, 1126, 1127, 1128, 1129, 1130 ),
          'orderby' => 'date',
          'order'   => 'ASC',
        ) 
        ); @endphp
        @php if ( $posttypes_query->have_posts() ) : while ( $posttypes_query->have_posts() ) : $posttypes_query->the_post(); @endphp
          <article class="latest-article">
            <h2 class="type-title">
            @php
              $obj = get_post_type_object( get_post_type( get_the_ID() ) );
              $posttype = $obj->labels->singular_name; 
              echo $posttype
            @endphp
            </h2>
            <h3> @php the_title(); @endphp</h3>
            @php the_excerpt(); @endphp
            <div class="latest-article-footer">
              @php /* echo get_avatar( get_the_author_email(), '128', '/images/no_images.jpg', get_the_author() ); */ @endphp
              @include('partials.authors')
            </div>
          </article>
          @php endwhile; endif; wp_reset_query(); 
        @endphp
    <div>
  </section>
