<!-- NAV BAR -->
<div class="primary-menu__wrapper">
    <div class="primary-menu">
        <nav class="navbar navbar-light navbar-expand-lg" role="navigation">
            <ul class="nav" >
            @php $locations = get_nav_menu_locations();
            // @if there's a location for the primary menu @endphp
            @if ( isset( $locations['primary_navigation']) )
                @php 
                    $menu = get_term( $locations['primary_navigation'], 'nav_menu');
                    // @if there are items in the primary menu
                @endphp
                @if ( $items = wp_get_nav_menu_items( $menu->name ) ) 
                    @php 
                        // loop through all menu items to display their content
                    @endphp
                    @foreach ( $items as $item )
                        @php
                            // @if the current item is not a top level item, skip it
                        @endphp
                        @if ($item->menu_item_parent != 0)
                            @php continue; @endphp
                        @endif
                        @php 
                            // get the ID of the current nav item
                            $curNavItemID = $item->ID;
                            // get the custom classes for the item
                            // (determined within the WordPress Appearance > Menu section)
                            $classList = implode(" ", $item->classes);
                            echo "<li class=\"{$classList}\">";
                            echo "<a href=\"{$item->url}\">{$item->title}</a>";
                            // build the mega-menu
                            // @if 'mega-menu'  exists within the class 
                        @endphp
                        @if ( in_array('has-mega-menu', $item->classes))
                            <div class="mega-menu__wrapper js-mega-menu">
                                <div class="mega-menu">
                                    <div class="mega-menu__content">
                                        <h2><?= $item->post_title; ?></h2>
                                        <p><?= $item->description; ?></p>
                                        <a href="<?= $item->url; ?>" class="learn-more">Read More</a>
                                    </div>
                                    <div class="mega-menu__subnav">
                                        <nav>
                                            <ul class="subnav">
                                            @foreach ( $items as $subnav)
                                                @if ( $subnav->menu_item_parent == $curNavItemID)
                                                    @php echo "<li><a href=\"{$subnav->url}\">{$subnav->title}</a></li>"; @endphp
                                                @endif
                                             @endforeach
                                            </ul>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        @endif
                        @php // @if this is the search bar @endphp
                        @if ( in_array('nav-search', $item->classes) )
                            <div class="search-bar__wrapper">
                                <div class="search-bar">
                                    <div class="search-bar__form">
                                        <form>
                                            <input type="text" name="s" placeholder="Keywords" />
                                            <button type="button" role="submit" name="Search">Search</button>
                                        </form>
                                        <a href="#" class="js-close-search search-bar__close"><svg role="img" class="icon"><use xlink:href="<?php bloginfo('template_url'); ?>/assets/dist/img/svg.svg#close"></use></svg></a>
                                    </div> <!-- /.search-bar__form -->
                                </div><!-- /.search-bar -->
                            </div><!-- /.search-bar__wrapper -->
                        @endif
                        @php echo '</li>'; @endphp
                    @endforeach
                @endif
            @endif
            </ul>
            @php //nationsu_primary_nav(); @endphp
        </nav>
    </div><!-- /.primary-menu -->
</div> <!-- /.primary-menu__wrapper -->