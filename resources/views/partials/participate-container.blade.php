
 <section id="participate" class="wrap participate-container">

    <div class="content container participate-title-holder">
      <h3 class="area-title">PARTICIPATE</h3>
    </div>
    <div class="wrap participate-body-holder">
      <div class="content container">
      <?php $participate_query = new WP_Query( 
          array(
            'p' => 189,  // participate ID
            'post_type' => 'any' // from any post type
          ) 
          );
          if ( $participate_query->have_posts() ) : while ( $participate_query->have_posts() ) : $participate_query->the_post(); ?>
            <article class="participate-article">
              @php the_excerpt() @endphp
              <a class="participate-button" href="<?php echo get_permalink(); ?>" >Participate</a>
            </article>
          <?php endwhile; endif; wp_reset_query(); ?>
      <div>
    </div>
  </section>
