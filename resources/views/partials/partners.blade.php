
<section id="partners" class="wrap partners-container">
  <div class="content container partners-title-holder">
    <h3 class="area-title">OUR PARTNERS</h3>
  </div>
  <div class="wrap partners-body-holder">
    <div class="content container">
      <?php  $loop = new WP_Query(
          array(
              'post_type' => 'Site Partners',
              'posts_per_page' => 50
            )
        );
        while ( $loop->have_posts() ) : $loop->the_post(); ?>
          <div class="partner-article-container">
            <article class="partner-article">
            <div class="partner-image">
              <?php the_post_thumbnail("medium"); ?>
            </div>
            <h4><?php the_title(); ?></h4>
            <?php the_excerpt(); ?>
            <a class="partner-button" href="<?php echo get_permalink(); ?>" >More Info</a>
          </div>
        <?php endwhile; wp_reset_query(); ?>
    <div>
  </div>
</section>