@php
  $categories = get_the_category();
@endphp
@if ( ! empty( $categories ) )
  <section class="categories-container"> 
    <p class="categories-title" >Categories: </p>
    <div  class="categories-links">
      @foreach( $categories as $category )
         <a class="cat-link" href="<?php echo get_category_link( $category->term_id ) ?>" alt="<?php echo sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ?>">@php echo $category->name @endphp</a>
      @endforeach
    </div>
  </section>
@else
  <p>Article has no category</p>
@endif
