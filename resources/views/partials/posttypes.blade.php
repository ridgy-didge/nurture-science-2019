<section id="posttypes" class="wrap posttypes-container">
  <article class="content container">
    <p class="area-title">ARTICLE TOPICS</p>
    @if (has_nav_menu('post_types_menu'))
      {!! wp_nav_menu(['theme_location' => 'post_types_menu', 'menu_class' => 'nav']) !!}
    @endif
  <article>
</section>
