
@php 
// fetch current post type to use as default value for search
$obj = get_post_type_object( get_post_type( get_the_ID() ) );
$posttype = $obj->labels->singular_name; 
@endphp

<section class="wrap search-container">
  <div class="content container">
    <div class="search-holder">
      <p class="area-title">Search Articles</p>
      <form role="search" method="get" class="searchform" action="<?php echo get_site_url() . '?s='?>">
        <input type="text" value="" name="s" id="s" aria-label="search button" placeholder="type here for search"/>
        <button aria-label="search button" type="submit" value="Search" class="search-button" >
          @include('svg.magnify-logo')
        </button>
      </form>
    </div>
  <div>
</section>
