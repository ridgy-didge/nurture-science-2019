<form method="post" action=""> 
    <select aria-label="sort filter" name="filter">
      <option value="date ASC" order="ASC" sort="date" <?php if(isset($_POST['filter']) && $_POST['filter'] == "date ASC") echo 'selected="selected"';?> >Date (ASC)</option>
      <option value="title ASC" order="ASC" sort="title" <?php if(isset($_POST['filter']) && $_POST['filter'] == "title ASC") echo 'selected="selected"';?> >Title (ASC)</option>
      <option value="author ASC" order="ASC" sort="author" <?php if(isset($_POST['filter']) && $_POST['filter'] == "author ASC") echo 'selected="selected"';?> >Author (ASC)</option>
      <option value="date DESC" order="DESC" sort="date" <?php if(isset($_POST['filter']) && $_POST['filter'] == "date DESC") echo 'selected="selected"';?> >Date (DESC)</option>
      <option value="title DESC" order="DESC" sort="title" <?php if(isset($_POST['filter']) && $_POST['filter'] == "title DESC") echo 'selected="selected"';?> >Title (DESC)</option>
      <option value="author DESC" order="DESC" sort="author" <?php if(isset($_POST['filter']) && $_POST['filter'] == "author DESC") echo 'selected="selected"';?> >Author (DESC)</option>
    </select>
  <input type="submit" name="submit" value="Sort Articles" class="btn-submit" />
</form>
