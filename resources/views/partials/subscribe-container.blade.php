
 <section id="subscribe" class="wrap subscribe-container">

    <div class="content container subscribe-title-holder">
      <h3 class="area-title">SUBSCRIBE</h3>
    </div>
    <div class="wrap subscribe-body-holder">
      <div class="content container">
      <?php $subscribe_query = new WP_Query( 
          array(
            'p' => 21,  // subscribe ID
            'post_type' => 'any' // from any post type
          ) 
          );
          if ( $subscribe_query->have_posts() ) : while ( $subscribe_query->have_posts() ) : $subscribe_query->the_post(); ?>
        <article class="subscribe-article">
          <?php the_excerpt(); ?>
          <p><?php echo do_shortcode( '[contact-form-7 id="78" title="Sign Up"]' ); ?></p>
        </article>
        <?php endwhile; endif; wp_reset_query(); ?>
      <div>
    </div>
  </section>
