<?php
  $tag = get_queried_object();
?>

<div class="wrap tags-wrap" role="document">

  <div class="container tags-container" role="document">
    <div class="tags-container-header">
      <div class="tags-container-title">
      <h3>Articles for Tag @php single_tag_title() @endphp</h3>
      </div>
      @include('partials.sort-form')
    </div>
    @php
      $selected_val = 'date'; // set inital state
      if(isset($_POST['submit'])){
      $selected_val = $_POST['filter'];  // update filter option
      }
    @endphp
    @php 
      $posttypes_query = new WP_Query( 
        array(
          'tag' => $tag->slug,
          'post_type' => array( 'pregnancy', 'newborns', 'baby', 'toddlers','yngadults', 'adults', 'elderly'),
          'post_status' => 'publish',
          'orderby' => $selected_val,
          'order'   => 'ASC',
        ) 
      ); 
    @endphp
    @php if ( $posttypes_query->have_posts() ) : while ( $posttypes_query->have_posts() ) : $posttypes_query->the_post(); @endphp
      <article class="tags-article">
        <h2 class="type-title">
        @php
          $obj = get_post_type_object( get_post_type( get_the_ID() ) );
          $posttype = $obj->labels->singular_name; 
          echo $posttype
        @endphp
        </h2>
        <h3>@php echo the_title() @endphp</h3>
        <div class="tags-article-excerpt">
          @php the_excerpt(); @endphp
        </div>
        <div class="tags-article-footer">
          @php /* echo get_avatar( get_the_author_email(), '128', '/images/no_images.jpg', get_the_author() ); */ @endphp
          @include('partials.authors')
        </div>
      </article>
      @php endwhile; endif; wp_reset_query(); 
    @endphp
  </div>
</div>
