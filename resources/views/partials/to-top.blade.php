<div class="toTop-container">
  <a aria-label="scroll back to the top of the page" id="toTop" class="btn-toTop" href="#">
    @include('svg.to-top')
  </a>
</div>