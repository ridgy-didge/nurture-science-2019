
 <section id="topics" class="wrap topics-container ">
    <article class="content container">
      <p class="area-title">Topics</p>
      <ul>
        @php 
        wp_list_categories( array(
            'orderby'    => 'name',
            'title_li' => '',
        ) );
        @endphp
      </ul>
    <article>
  </section>
