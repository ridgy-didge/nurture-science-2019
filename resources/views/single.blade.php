@extends('layouts.app')
@php
  $articleID = get_the_ID();
  $aboutIDs = $articleID == 1118 ||  $articleID == 1120 ||  $articleID == 1122 ||  $articleID == 1124 ||  $articleID == 1126 ||  $articleID == 1127 ||  $articleID == 1128 ||  $articleID == 1129 ||  $articleID == 1130;
@endphp
@section('content')
  <section class="wrap welcome-wrap">
    <article class="content container">
      @while(have_posts()) @php the_post() @endphp
        @include('partials.page-header')
        <img alt="welcome background image" class="welcome-bg-image" src="@asset('images/nurture-science-white.svg')" />
        <div class="welcome-info">
          <div class="archive-author-topic">
            <p>Topic: 
              @php
                $pt = get_post_type_object( get_post_type() ); 
                $labelTitle = $pt->labels->singular_name;
                echo $labelTitle; 
              @endphp
            </p>
          </div>
          @if ( $aboutIDs || $labelTitle == 'Site Partner' )
            @php // show nothing @endphp
            @else
            @include('partials.authors')
          @endif
        </div>
      @endwhile
    </article>
  </section>
  @if ( $labelTitle == 'Site Partner' )
    <section class="wrap article-wrap">
      <article class="content container">
        @include('partials.content-partner-'.get_post_type())
      </article>
    </section>
  @endif
  @if ( $aboutIDs )
    <section class="wrap article-wrap">
      <article class="content container">
        @include('partials.content-about-posttype-'.get_post_type())
      </article>
    </section>
    @include('partials.latestarticles-posttype')
  @else 
  <section class="wrap article-wrap">
    <article class="content container">
      @include('partials.content-single-'.get_post_type())
    </article>
  </section>
  @endif
  

  @if ( $aboutIDs )
    @include('partials.archives-list')
  @endif
  
  @include('partials.search')
  @include('partials.donate-container')
  @include('partials.latestarticles')
  @include('partials.contact-container')
  @include('partials.posttypes')
  @include('partials.participate-container')
  @include('partials.subscribe-container')
@endsection