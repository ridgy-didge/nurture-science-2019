{{--
  Template Name: About Us Template
--}}

@extends('layouts.app')

@section('content')
  <section class="wrap welcome-wrap">
    <article class="content container">
      @while(have_posts()) 
        @php the_post() @endphp
        @include('partials.page-header')
        <img class="welcome-bg-image" src="@asset('images/nurture-science-white.svg')" />
      @endwhile
    </article>
  </section>
  <section class="wrap mission-wrap">
    <article class="content container">
      @include('partials.content-page')
    </article>
  </section>
  <section class="wrap the-team-container">
    <article class="content container">
      @php
        $post_the_team = get_page_by_title( 'The Team' );
        $content = $post_the_team->post_content;
        $title = $post_the_team->post_title
      @endphp
      <h4>@php echo $title @endphp</h4>
      @php echo $content @endphp
    </article>
  </section>
  <section class="wrap the-team-container">
    <div class="site-authors-list container">
    @php $blogadmins = get_users( 'blog_id=1&orderby=nicename&role=editor' ) @endphp
      <h3>Site Authors</h3>
      @foreach ( $blogadmins as $user ) 
      @php $archive_link = get_author_posts_url( $user->ID, $user->user_nicename ); @endphp
          <a class="" href="<?php echo esc_url( $archive_link )?>">
            <div href=<?php echo esc_url( $archive_link )?>" class="" >
              <div class="" >
                <h5 class="">
                   @php echo esc_html( $user->display_name ) @endphp 
                </h5>
                <p class=""> @php echo esc_html( $user->description ) @endphp </p>
              </div>
            </div>
          </a>
      @endforeach
    </div>
  </section>
  <section class="wrap the-team-container">
    <div class="site-authors-list container">
    @php $blogadmins = get_users( 'blog_id=1&orderby=nicename&role=author' ) @endphp
      <h3>Site Authors</h3>
      @foreach ( $blogadmins as $user ) 
      @php $archive_link = get_author_posts_url( $user->ID, $user->user_nicename ); @endphp
          <a class="" href="<?php echo esc_url( $archive_link )?>">
            <div href=<?php echo esc_url( $archive_link )?>" class="" >
              <div class="" >
                <h5 class="">
                   @php echo esc_html( $user->display_name ) @endphp 
                </h5>
                <p class=""> @php echo esc_html( $user->description ) @endphp </p>
              </div>
            </div>
          </a>
      @endforeach
    </div>
  </section>

  <section class="wrap site-admins-container">
    <div class="site-admins-list-container container">
      <?php $blogadmins = get_users( 'blog_id=1&orderby=nicename&role=administrator' ); ?>
      <h3>Site Administrators</h3>
      <div class="site-admins-list">
        @foreach ( $blogadmins as $user )
            <?php $archive_link = get_author_posts_url( $user->ID, $user->user_nicename ); ?>
            <div class="admin-container" >
              <a class="admin-link" href="<?php echo esc_url( $archive_link )?>">
                  <?php echo esc_html( $user->display_name ) ?>
              </a>
              <p class=""> <?php echo esc_html( $user->description ) ?>
            </div>
        @endforeach
       </div>
    </div>
  </section>
  @include('partials.partners')
  @include('partials.search')
  @include('partials.donate-container')
  @include('partials.latestarticles')
  @include('partials.contact-container')
  @include('partials.posttypes')
  @include('partials.participate-container')
  @include('partials.subscribe-container')

@endsection
