{{--
  Template Name: Homepage Template
--}}

@extends('layouts.app')

@section('content')
  <section class="wrap welcome-wrap" role="document">
      <article class="content container">
        @while(have_posts()) @php the_post() @endphp
          @include('partials.page-header')
          @include('partials.content-page')
          <img class="welcome-bg-image" alt="welcome background image" src="@asset('images/nurture-science-white.svg')" />
        @endwhile
      </article>
  </section>
  @include('partials.posttypes')
  @include('partials.search')
  @include('partials.latestarticles')
  @include('partials.contact-container')
  @include('partials.donate-container')
  @include('partials.participate-container')
  @include('partials.subscribe-container')
@endsection
