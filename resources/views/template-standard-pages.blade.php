{{--
  Template Name: Standard Page Template
--}}

@extends('layouts.app')

@section('content')
  <section class="wrap welcome-wrap">
    <article class="content container">
      @while(have_posts()) @php the_post() @endphp
      @include('partials.page-header')
      <img class="welcome-bg-image" src="@asset('images/nurture-science-white.svg')" />
      @endwhile
    </article>
  </section>
  <section class="wrap mission-wrap">
    <article class="content container">
      @include('partials.content-page')
    </article>
  </section>

  @include('partials.partners')
  @include('partials.search')
  @include('partials.donate-container')
  @include('partials.latestarticles')
  @include('partials.contact-container')
  @include('partials.posttypes')
  @include('partials.participate-container')
  @include('partials.subscribe-container')
@endsection
